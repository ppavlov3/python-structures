"""
Calculate n!. n! = 1 * 2 * 3 * … * (n-1) * n,  0! = 1. n >= 0.
"""


def main():
    """Factorial calculation."""
    n = int(input())

    i = 1
    k = 1
    while i <= n:
        k *= i
        i += 1
    print(k)


if __name__ == "__main__":
    main()
