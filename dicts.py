"""
 Drop empty items from a dictionary.
"""
import json


def main():
    """Drop empty items from a dictionary."""
    d = json.loads(input())
    dc = d.copy()
    
    for key, value in dc.items():
        if value == None:
            del d[key]

    print(d)


if __name__ == "__main__":
    main()
