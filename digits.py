"""
Find sum of n-integer digits. n >= 0.
"""


def main():
    """Sum of number digits."""
    n = str(input())


    if int(n) == 0:
        k = 0
        print(k)
    elif int(n) > 0:
        i = 0
        k = 0
        while i < len(n):
            k += int(n[i])
            i += 1
        print(k)
    else: print("Nothing")


if __name__ == "__main__":
    main()
