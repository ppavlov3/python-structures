def main():
 
    my_str = input().split(" ")
    re_str = " ".join([ i[::-1] for(i) in my_str ])

    print(re_str)

if __name__ == "__main__":
    main()