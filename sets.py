"""
Find common items in 2 lists without duplicates. Sort the result list before output.
"""


def main():
    """Find common numbers."""
    li1 = list(map(int, input().split()))
    li2 = list(map(int, input().split()))

    my_list = sorted((set(li1) & set(li2)))
    print(my_list)


if __name__ == "__main__":
    main()
