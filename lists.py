"""
Consider a list (list = []). You can perform the following commands:
insert i e: Insert integer e at position i.
print: Print the list.
remove e: Delete the first occurrence of integer e.
append e: Insert integer e at the end of the list.
sort: Sort the list.
pop: Pop the last element from the list.
reverse: Reverse the list.

Initialize your list and read in the value of followed by lines of commands
where each command will be of the  types listed above. Iterate through each command
in order and perform the corresponding operation on your list.
The first line contains an integer, denoting the number of commands.
Each line  of the  subsequent lines contains one of the commands described above.
"""


def main():
    """Perform list commands."""
    
    n = int(input("How many commands? "))

    my_list = []

    for i in range(0, n):
        command_run = str(input("Enter command: ")).split(" ")
        my_case = command_run[0]
        if my_case == "insert":
            my_list.insert(int(command_run[1]), int(command_run[2]))
        elif my_case == "remove":
            if int(command_run[1]) in my_list:
                my_list.remove(int(command_run[1]))
            else: print("Not found!")
        elif my_case == "append":
            my_list.append(int(command_run[1]))
        elif my_case == "sort":
            my_list.sort()
        elif my_case == "pop":
            my_list.pop()
        elif my_case == "reverse":
            my_list.reverse()
        elif my_case == "print":
            print(my_list)
        
        i += 1


if __name__ == "__main__":
    main()
