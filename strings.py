"""
Check whether the input string is palindrome.
"""


def main():
    """Check palindrome."""
    s = input()

    rs = s[::-1]

    if s == rs:
        print("yes")
    else:
        print("no")

if __name__ == "__main__":
    main()
